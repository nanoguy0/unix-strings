# Unix Strings
A NodeJS implementation of the unix utility strings, which dumps all the `string content` of a file. Useful for `decompilers`. Contains no dependencies. 

# Example

```javascript

const stringExtractor = require('unix-strings');
const fs = require('fs');

// Give it a file path
stringExtractor('path/to/some/file.exe', function (chunk, offset, fileStream) {
    console.log(chunk.toString()); // Output the string buffer to the console
    fileStream.pause(); // See NodeJS Docs: ReadableStream
})
// Returns a promise with the final result
.then(function (extracted) {
    // The entire buffer
    fs.writeFileSync('path/to/some/output.txt', extracted.toString());
});

// Use a already initalized file stream
var existingStream = fs.createReadStream('path/to/some/file.exe');
stringExtractor(existingStream, ...);

```

```bash
node node_modules/unix-strings/index.js path/to/some/file.exe
```


# Notes

In order to make byte checking faster, some `WebAssembly` is used. 


### C Code
```c
bool STRING_ISGRAPHIC(int c) {
  return ((c) >= 0 && (c) <= 255 && ((c) == '\t' || ((c >= ' ' && c <= '~') ? 1 : 0)));
}
```

### WebAssemblyCode
```wasm
(module
  (type $type0 (func (param i32) (result i32)))
  (table 0 anyfunc)
  (memory 1)
  (export "memory" memory)
  (export "_Z16STRING_ISGRAPHICi" $func0)
  (func $func0 (param $var0 i32) (result i32)
    (local $var1 i32) 
    i32.const 0
    set_local $var1
    block $label0
      get_local $var0
      i32.const 255
      i32.gt_u
      br_if $label0
      i32.const 1
      set_local $var1
      get_local $var0
      i32.const 9
      i32.eq
      br_if $label0
      get_local $var0
      i32.const -32
      i32.add
      i32.const 95
      i32.lt_u
      set_local $var1
    end $label0
    get_local $var1
  )
)
```

***If you want to disable it and use the javascript version, just set a global variable before you require the file:***

```javascript
global._unixStringsAssembly = false;
const stringExtractor = require('unix-strings');
```