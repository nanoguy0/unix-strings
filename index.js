/**
 *  @name unixStrings
 *  @description Javascript Implementation of UNIX's `strings` utility
 *  @author Benjamin Bartrim 2020
 *  @version 1.0.0
 *  @todo Use faster buffers over arrays, cli args
 */

const useAssembly = typeof global._unixStringsAssembly != 'undefined' ? global._unixStringsAssembly : false;

var isascii, isprint, ISPRINT, STRING_ISGRAPHIC, WS_MOD;

const encoding = 's'; // {s,S,b,l,B,L}
const string_min = 3; // Minimum length of sequence of graphic chars to trigger output. 



if (useAssembly) {
    (async () => {
        /** Assembly code for checking the characters
            C_CODE: 
                bool STRING_ISGRAPHIC(int c) { return ((c) >= 0 && (c) <= 255 && ((c) == '\t' || ((c >= ' ' && c <= '~') ? 1 : 0))); }
        */
        WS_MOD = await WebAssembly.instantiate(Buffer.from([0, 97, 115, 109, 1, 0, 0, 0, 1, 134, 128, 128, 128, 0, 1, 96, 1, 127, 1, 127, 3, 130, 128, 128, 128, 0, 1, 0, 4, 132, 128, 128, 128, 0, 1, 112, 0, 0, 5, 131, 128, 128, 128, 0, 1, 0, 1, 6, 129, 128, 128, 128, 0, 0, 7, 162, 128, 128, 128, 0, 2, 6, 109, 101, 109, 111, 114, 121, 2, 0, 21, 95, 90, 49, 54, 83, 84, 82, 73, 78, 71, 95, 73, 83, 71, 82, 65, 80, 72, 73, 67, 105, 0, 0, 10, 177, 128, 128, 128, 0, 1, 171, 128, 128, 128, 0, 1, 1, 127, 65, 0, 33, 1, 2, 64, 32, 0, 65, 255, 1, 75, 13, 0, 65, 1, 33, 1, 32, 0, 65, 9, 70, 13, 0, 32, 0, 65, 96, 106, 65, 223, 0, 73, 33, 1, 11, 32, 1, 11]));
        STRING_ISGRAPHIC = WS_MOD.instance.exports._Z16STRING_ISGRAPHICi;
    })();
} else {
    // Internals
    isascii = c => 1; //(c >= 0) && (c <= 127); // Implementation pending
    isprint = c => (c > 31) && (c < 127);

    // Headers
    ISPRINT = c => (isascii(c) && isprint(c));
    STRING_ISGRAPHIC = c => ((c) > 0 && (c) < 266 && ((c) == '\t'.charCodeAt(0) || ISPRINT(c) || (encoding == 'S' && (c) > 127)));
}


module.exports =
    function print(file, ondata) {
        let final = [];
        let abuff = [];
        let fileIndex = 0;
        return new Promise(async resolve => {
            while (!STRING_ISGRAPHIC) await new Promise(_ => setTimeout(_, 1)); // Wait for variable to come up
            var inStream = (typeof file == 'string' ? require('fs').createReadStream(file) : file)
                .on('data', buff => {
                    for (let x of buff) {
                        fileIndex++;
                        if (STRING_ISGRAPHIC(x)) { abuff.push(x); continue; }
                        else {
                            if (abuff.length > string_min) { if (ondata) ondata(Buffer.from(abuff), fileIndex, inStream); final = final.concat(abuff); final.push(10); }
                            if (abuff.length != 0) abuff = [];
                        }
                    }
                })
                .on('error', e => {
                    console.log('Failed to print strings got error: ', e);
                    process.exit(1);
                })
                .on('end', _ => { resolve(Buffer.from(final)) });
        })
    };

 
// If the file is run directly
if (require.main === module) if (process.argv.length < 3) console.log('No file provided'); else module.exports(process.argv[2], _ => console.log(_.toString()));